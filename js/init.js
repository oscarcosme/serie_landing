var init = (function () {


	//VARIABLES
	var group1 = new TimelineMax({paused:true});
		group2 = new TimelineMax({paused:true});
		group10 = new TimelineMax({paused:true});
		group11 = new TimelineMax({paused:true});
		group12 = new TimelineMax({paused:true});
		group13 = new TimelineMax({paused:true});
		group14 = new TimelineMax({paused:true});
		group15 = new TimelineMax({paused:true});
		
	var player;

	//INICIAR ANIMACIONES

	function iniciar(tab) {
		//Destruimos la animación para mobile
		var isMobile = window.matchMedia("screen and (max-width: 760px)");

		switch(tab) {
			case "home":
				new Waypoint({
					element: document.getElementsByClassName('waypoint-1'),
					handler: function(direction) {
						if(direction === 'right'){
							group1.play();
							console.log(direction);
						}else if (direction === 'left'){
							group1.reverse();
							console.log(direction);
						}
					},
					context: document.getElementsByClassName('scroll-serie'),
					horizontal: true,
					offset: '30%'

				});
				
				break;

			case "episode_01":
				//var waypoint = new Waypoint({
				new Waypoint({
					element: document.getElementsByClassName('waypoint-1'),
					handler: function(direction) {
						if(direction === 'right'){
							group11.play();
							// console.log(direction);
						}else if (direction === 'left'){
							group11.reverse();
							// console.log(direction);
						}
					},
					context: document.getElementsByClassName('scroll-serie'),
					horizontal: true,
					offset: '80%'

				});

				new Waypoint({
					element: document.getElementsByClassName('waypoint-2'),
					handler: function(direction) {
						if(direction === 'right'){
							group12.play();
							// console.log(direction);
						}else if (direction === 'left'){
							group12.reverse();
							// console.log(direction);
						}
					},
					context: document.getElementsByClassName('scroll-serie'),
					horizontal: true,
					offset: '80%'

				});

				new Waypoint({
					element: document.getElementsByClassName('waypoint-3'),
					handler: function(direction) {
						if(direction === 'right'){
							group13.play();
							// console.log(direction);
						}else if (direction === 'left'){
							group13.reverse();
							// console.log(direction);
						}
					},
					context: document.getElementsByClassName('scroll-serie'),
					horizontal: true,
					offset: '75%'

				});

				new Waypoint({
					element: document.getElementsByClassName('waypoint-4'),
					handler: function(direction) {
						if(direction === 'right'){
							group14.play();
							// console.log(direction);
						}else if (direction === 'left'){
							group14.reverse();
							// console.log(direction);
						}
					},
					context: document.getElementsByClassName('scroll-serie'),
					horizontal: true,
					offset: '80%'

				});

				new Waypoint({
					element: document.getElementsByClassName('waypoint-5'),
					handler: function(direction) {
						if(direction === 'right'){
							group15.play();
							// console.log(direction);
						}else if (direction === 'left'){
							group15.reverse();
							// console.log(direction);
						}
					},
					context: document.getElementsByClassName('scroll-serie'),
					horizontal: true,
					offset: '75%'

				});

				break;
			
		}
	}
	

	//ANIMANDO
	/*ANIMACION DEL HOME*/
	group1
		.from(".slider .container span.h1", 1, {opacity:0, y:-20, ease:Back.easeOut}, 0.1)
		.from(".slider .container span.h2", 1, {opacity:0, y:20, ease:Back.easeOut}, 0.5)
		.from(".slider .container .frase", 1, {opacity:0, x:-20, ease:Back.easeOut}, 1)
		.from(".slider .container .button-more", 1, {opacity:0, y:50, ease:Back.easeOut}, 1.5)
		.from(".slider .container ul .one", 0.5, {opacity:0, y:10, ease:Back.easeOut}, 2)
		.from(".slider .container ul .two", 0.5, {opacity:0, y:10, ease:Back.easeOut}, 2.3)
		.from(".slider .container ul .three", 0.5, {opacity:0, y:10, ease:Back.easeOut}, 2.6)

	/*ANIMACION DEL EPISODIO*/
	
	group11
		.from(".scroll-serie .box-1 .p02", 10, {x:200}, 1)
		.to(".scroll-serie .box-1 .p02", 10, {x:-100}, 5)

		.from(".scroll-serie .box-1 .p03", 7, {x:100}, 1.5)

		.from(".scroll-serie .box-1 .p04", 7, {x:200}, 2)
		.to(".scroll-serie .box-1 .p04", 7, {x:-100}, 7)

		.from(".scroll-serie .box-1 .p04_1", 7, {x:200}, 2)
		.to(".scroll-serie .box-1 .p04_1", 7, {x:-100}, 7)

		.from(".scroll-serie .box-1 .p05", 7, {x:100}, 3)
		.from(".scroll-serie .box-1 .p06", 7, {x:100}, 4)
		
		.from(".scroll-serie .box-1 .p07_1", 7, {x:200}, 4)
		.to(".scroll-serie .box-1 .p07_1", 7, {x:100}, 9)
		
		.from(".scroll-serie .box-1 .p07_2", 7, {x:200}, 4)
		.to(".scroll-serie .box-1 .p07_2", 7, {x:100}, 9)
		
		.from(".scroll-serie .box-1 .p08", 7, {x:100}, 6)

	group12
		.from(".scroll-serie .box-2 .ms_01", 10, {x:200}, 1)
		.from(".scroll-serie .box-2 .ms_02", 15, {x:150}, 3)
		.from(".scroll-serie .box-2 .ms_03", 10, {x:150}, 5)
		.from(".scroll-serie .box-2 .p10", 10, {x:50}, 5)
		.from(".scroll-serie .box-2 .ms_04", 5, {x:50}, 7)
		.from(".scroll-serie .box-2 .ms_05", 5, {x:50}, 9)
		.from(".scroll-serie .box-2 .ms_06", 5, {x:20}, 11)
		.from(".scroll-serie .box-2 .ms_07", 5, {x:10}, 11)

	group13
		.from(".scroll-serie .box-3 .p20", 7, {x:300}, 2)
		.from(".scroll-serie .box-3 .p21", 10, {x:500}, 5)
		.from(".scroll-serie .box-3 .p23", 10, {x:200}, 1)
		.from(".scroll-serie .box-3 .p22", 10, {x:600}, 7)

	group14
		.from(".scroll-serie .box-4 .p25", 7, {x:150}, 1)
		.to(".scroll-serie .box-4 .p25", 10, {x:-50}, 5)
		.from(".scroll-serie .box-4 .text-004", 7, {x:20}, 3)

	group15
		.from(".scroll-serie .box-5 .p31", 7, {x:150}, 1)
		.from(".scroll-serie .box-5 .p32", 7, {x:150}, 3)
		.from(".scroll-serie .box-5 .p33", 7, {x:150}, 5)
		.from(".scroll-serie .box-5 .p34", 7, {x:150}, 3)
		.from(".scroll-serie .box-5 .p35", 7, {x:150}, 5)
		.from(".scroll-serie .box-5 .p36", 5, {x:130}, 7)
		
		.from(".scroll-serie .box-5 .text6", 5, {x:130}, 7)
		.from(".scroll-serie .box-5 .text7", 5, {x:160}, 8)
	


	//INICIAMOS LA ANIMACION POR DEFAUL
	iniciar('home');

	//END


	$('.slider .button-more a').on('click', function(e){
		e.preventDefault();
		// $('.slider .container').fadeOut('slow');
		// $('.scroll-serie').fadeIn('slow');

		Waypoint.destroyAll();

		// group11.pause(0);
		// iniciar('episode_01');

	})



	
	var $slider = $(".landing-wrapper");
	var $content = $(".landing-inner-content");
	var mytween;

	
	var tl = new TimelineMax({paused: true});
	
	// $('.scroll-serie .arrow img').on('click', function(){


	// })

		// scroll left and right
		$content.on("mousemove", function(e) {
			if (e.clientX > $slider.width() / 2) {
				mytween = TweenMax.to($slider, 8, {
				scrollTo: { x: "+=250"}, ease: Power2.easeOut
				});

				setTimeout(function(){ $('.scroll-serie .arrow').fadeOut('slow'); }, 2000);
				
				group11.play();
				group12.play();
				group13.play();
				group14.play();
				group15.play();

			} else {
				mytween = TweenMax.to($slider, 8, {scrollTo: {x: "-=250"}, ease: Power2.easeOut});

				setTimeout(function(){ $('.scroll-serie .arrow').fadeIn('slow'); }, 2000);

				group11.reverse();
				group12.reverse();
				group13.reverse();
				group14.reverse();
				group15.reverse();
			}
		});	
	
	
	
	




})();